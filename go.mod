module bam

go 1.15

require (
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/pquerna/ffjson v0.0.0-20190930134022-aa0246cd15f7
	github.com/spf13/cobra v1.1.1 // indirect
	golang.org/x/sys v0.0.0-20201024232916-9f70ab9862d5
)
