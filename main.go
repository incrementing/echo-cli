package main

import (
	"bam/cmd"
	"os"
)

/*
CLI tool used in Minecraft cheat detection software.
TODO:
- Clean up (code is super messy)
- Log/catch errors
- Help command?
*/

func main() {
	if len(os.Args) < 2 || len(os.Args) > 3 {
		os.Exit(1)
	}

	switch os.Args[1] {

	// Parse BAM registry values
	case "bam":
		cmd.BAM()
		os.Exit(0)

	// TODO: Make this work
	//// Parse User Assist registry values
	//case "ua":
	//	cmd.UA()
	//	os.Exit(0)

	// Takes path to the minecraft resourcepacks directory
	// returns a json serialized summary of the first 10 packs
	// Supports packs in directories or zip files
	case "packs":
		if len(os.Args) != 3 {
			os.Exit(1)
		}
		cmd.PACKS(os.Args[2])
		os.Exit(0)

	// Last modification time of the bin
	case "bin":
		cmd.BIN()
		os.Exit(0)

	default:
		os.Exit(1)
	}
}
