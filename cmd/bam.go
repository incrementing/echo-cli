package cmd

import (
	"encoding/binary"
	"errors"
	"fmt"
	"golang.org/x/sys/windows/registry"
	"strconv"
	"syscall"
	"time"
)

func BAM() {
	bamKey := `SYSTEM\CurrentControlSet\Services\bam\UserSettings\`

	output := make(map[string]int64)
	k, err := registry.OpenKey(registry.LOCAL_MACHINE, `SYSTEM\CurrentControlSet\Services\bam\UserSettings`, registry.QUERY_VALUE|registry.ENUMERATE_SUB_KEYS)
	if err != nil {
		if err.Error() == "The system cannot find the file specified." {
			k, err = registry.OpenKey(registry.LOCAL_MACHINE, `SYSTEM\CurrentControlSet\Services\bam\State\UserSettings`, registry.QUERY_VALUE|registry.ENUMERATE_SUB_KEYS)
			bamKey = `SYSTEM\CurrentControlSet\Services\bam\State\UserSettings\`
			if err != nil {
				fmt.Println("can't open registry key", err)
				return
			}
		}
	}
	defer k.Close()

	subkeys, err := k.ReadSubKeyNames(0)
	if err != nil {
		fmt.Printf("can't read child key name %#v\n", err)
		return
	}

	for _, subkey := range subkeys {
		subKey, err := registry.OpenKey(registry.LOCAL_MACHINE, bamKey+subkey, registry.QUERY_VALUE)
		if err != nil {
			fmt.Println("can't open sub registry key", subkey, err)
			return
		}

		params, err := subKey.ReadValueNames(0)
		if err != nil {
			fmt.Printf("can't read child key param %s %#v\n", subkey, err)
			return
		}
		for _, param := range params {
			val, err := getRegistryValueAsString(subKey, param)
			if err != nil {
				//fmt.Println(err)
				continue
			}
			output[param] = val
		}
		subKey.Close()
	}

	for k, v := range output {
		fmt.Println(k + "|" + strconv.FormatInt(v, 10))
	}
}

func getRegistryValueAsString(key registry.Key, subKey string) (int64, error) {
	val, _, err := key.GetBinaryValue(subKey)
	if err != nil {
		return -1, errors.New("Can't get type for child key " + subKey)
	}
	// Contains a 64-bit value representing the number of 100-nanosecond intervals since January 1, 1601 (UTC).
	ft := &syscall.Filetime{
		LowDateTime:  binary.LittleEndian.Uint32(val[:4]),
		HighDateTime: binary.LittleEndian.Uint32(val[4:]),
	}
	return time.Unix(0, ft.Nanoseconds()).Unix(), nil
}
