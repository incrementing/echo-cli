package cmd

import (
	"fmt"
	"github.com/pquerna/ffjson/ffjson"
	"math"
	"os"
	"os/user"
	"syscall"
	"time"
)

func BIN() {
	usr, err := user.Current()
	if err != nil {
		fmt.Println(err)
	}

	sid, _, _, err := syscall.LookupSID("", usr.Username)
	if err != nil {
		fmt.Println(err)
	}
	sidStr, err := sid.String()
	if err != nil {
		fmt.Println(err)
	}

	file, err := os.Open(os.Getenv("systemdrive") + "\\$Recycle.Bin\\" + sidStr)
	if err != nil {
		fmt.Println(err)
	}
	stat, err := file.Stat()
	if err != nil {
		fmt.Println(err)
	}

	binJson, err := ffjson.Marshal(struct {
		ModTime int64 `json:"modTime"`
		ModAgo  int64 `json:"modAgo"`
	}{stat.ModTime().Unix(), int64(math.Floor(time.Since(stat.ModTime()).Seconds()))})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(binJson))
}
