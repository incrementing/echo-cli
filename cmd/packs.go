package cmd

import (
	"archive/zip"
	"bufio"
	"bytes"
	"encoding/base64"
	"fmt"
	"github.com/nfnt/resize"
	"github.com/pquerna/ffjson/ffjson"
	"image/png"
	"io/ioutil"
	"os"
	"strings"
)

type Pack struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	PackPNG     string `json:"packPNG"`
}

type PackMeta struct {
	Pack struct {
		Format      int    `json:"pack_format"`
		Description string `json:"description"`
	} `json:"pack"`
}

func PACKS(path string) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		fmt.Println(err)
	}

	var parsedPacks []*Pack

	packsProcessed := 0
	for _, file := range files {

		if packsProcessed >= 10 {
			break
		}

		if file.IsDir() {
			packFolder, err := ioutil.ReadDir(path + file.Name())
			if err != nil {
				continue
			}

			if validPack(packFolder) {
				pack := parsePack(file.Name(), path, packFolder)
				if pack != nil {
					parsedPacks = append(parsedPacks, pack)
					packsProcessed++
				}
			}
		} else if strings.HasSuffix(strings.ToLower(file.Name()), ".zip") {
			zipReader, err := zip.OpenReader(path + file.Name())
			if err != nil {
				continue
			}

			if validCompressedPack(zipReader.File) {
				pack := parseCompressedPack(file.Name(), zipReader.File)
				if pack != nil {
					parsedPacks = append(parsedPacks, pack)
					packsProcessed++
				}
			}
		}
	}

	packJson, _ := ffjson.Marshal(parsedPacks)
	fmt.Println(string(packJson))
}

func parsePack(name, path string, contents []os.FileInfo) *Pack {
	pack := &Pack{Name: name}

	for _, content := range contents {
		if strings.ToLower(content.Name()) == "pack.mcmeta" {
			var meta PackMeta
			metaContents, err := ioutil.ReadFile(path + name + "\\" + content.Name())
			if err != nil {
				return nil
			}
			if err := ffjson.Unmarshal(metaContents, &meta); err != nil {
				return nil
			}
			pack.Description = meta.Pack.Description
		} else if strings.ToLower(content.Name()) == "pack.png" {
			pngReader, err := os.Open(path + name + "\\" + content.Name())
			if err != nil {
				pngReader.Close()
				return nil
			}

			img, err := png.Decode(pngReader)
			if err != nil {
				pngReader.Close()
				return nil
			}

			pngReader.Close()
			pngReader, err = os.Open(path + name + "\\" + content.Name())
			if err != nil {
				pngReader.Close()
				return nil
			}

			imgConf, err := png.DecodeConfig(pngReader)
			if err != nil {
				pngReader.Close()
				return nil
			}

			if imgConf.Height > 128 || imgConf.Width > 128 {
				img = resize.Resize(128, 128, img, resize.Lanczos3)
			}

			var buf bytes.Buffer
			bufio.NewWriter(&buf)
			png.Encode(&buf, img)
			pack.PackPNG = "data:image/png;base64," + base64.StdEncoding.EncodeToString(buf.Bytes())
			pngReader.Close()
		}
	}

	return pack
}

func parseCompressedPack(name string, contents []*zip.File) *Pack {
	pack := &Pack{Name: strings.Replace(name, ".zip", "", 1)}

	for _, content := range contents {
		if strings.ToLower(content.Name) == "pack.mcmeta" {
			var meta PackMeta
			metaContents, err := content.Open()
			if err != nil {
				return nil
			}

			metaBytes := new(bytes.Buffer)
			metaBytes.ReadFrom(metaContents)

			if err := ffjson.Unmarshal(metaBytes.Bytes(), &meta); err != nil {
				metaContents.Close()
				return nil
			}
			metaContents.Close()
			pack.Description = meta.Pack.Description
		} else if strings.ToLower(content.Name) == "pack.png" {
			compressedPngReader, err := content.Open()
			if err != nil {
				compressedPngReader.Close()
				return nil
			}

			img, err := png.Decode(compressedPngReader)
			if err != nil {
				compressedPngReader.Close()
				return nil
			}
			compressedPngReader.Close()

			compressedPngReader, err = content.Open()
			if err != nil {
				compressedPngReader.Close()
				return nil
			}

			imgConf, err := png.DecodeConfig(compressedPngReader)
			if err != nil {
				fmt.Println(err.Error())
				compressedPngReader.Close()
				return nil
			}

			if imgConf.Height > 128 || imgConf.Width > 128 {
				img = resize.Resize(128, 128, img, resize.Lanczos3)
			}

			var buf bytes.Buffer
			bufio.NewWriter(&buf)
			png.Encode(&buf, img)
			pack.PackPNG = "data:image/png;base64," + base64.StdEncoding.EncodeToString(buf.Bytes())
			compressedPngReader.Close()
		}
	}

	return pack
}

func validPack(files []os.FileInfo) bool {
	for _, file := range files {
		if strings.ToLower(file.Name()) == "pack.mcmeta" {
			return true
		}
	}
	return false
}

func validCompressedPack(files []*zip.File) bool {
	for _, file := range files {
		if strings.ToLower(file.Name) == "pack.mcmeta" {
			return true
		}
	}
	return false
}
